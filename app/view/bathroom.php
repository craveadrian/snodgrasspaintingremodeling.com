<div id="content">
	<div class="row">
		<h1>BATHROOM REMODEL</h1>
    <div class="inner-bathromm-remodel">
			<div class="desc">
				<h2>Treat yourself to the relaxing bathroom of your dreams.</h2>
				<p>Sometimes, your bathroom needs to be as much of a sanctuary as your bedroom. Give yourself the luxurious bathroom you've always wanted with the best quality brands avaiable of faucets, baths, showers and more.</p>
				<p>From new fixtures and cabinet hardware to large-scale overhauls, the crew at Snodgrass Painting & Remodeling will work hard to turn your run-of-the-mill bathroom into a serene and enjoyable environment that will have you loving your new remodel.</p>
			</div>
			<div class="desc">
				<h2>Bring your bathroom up to speed</h2>
				<p>If your blast-from-the-past bathroom doesn’t work well with the rest of your home, we can help. No matter what your updating needs, our knowledgeable crew will provide you with the fixtures, storage options and appliances you need for an enjoyable experience.</p>
				<p>Want to keep some of that antique quality? Our experienced team can inspect your old bathroom and let you know what can stay and what should go and keep your bathroom looking and working great.</p>
			</div>
    </div>
		<div id="gall1" class="gallery-container">
		<ul class="gallery clearfix" >
				<?php foreach ($gallery as $gall) {  if($gall["rel"] == "bathroom") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
	</div>
</div>
