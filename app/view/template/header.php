<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<a href="<?php echo URL ?>"> <img src="public/images/common/mainLogo.png" alt="SNODGRASS PAINTING & REMODELING MAIN LOGO" class="mainLogo"> </a>
			<div class="hdTop">
				<div class="row">
					<p class="inbMid">
						<a href="<?php $this->info("fb_link"); ?>" class="socialico">f</a>
						<a href="<?php $this->info("tt_link"); ?>" class="socialico">l</a>
						<a href="<?php $this->info("yt_link"); ?>" class="socialico">x</a>
						<a href="<?php $this->info("rss_link"); ?>" class="socialico">r</a>
					</p>
					<img src="public/images/common/phone.png" alt="Phone Icon" class="inbMid">
					<p class="phone inbMid"><?php $this->info(["phone","tel"]) ?></p>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("painting"); ?>><a href="<?php echo URL ?>painting#content">PAINTING</a></li>
							<li <?php $this->helpers->isActiveMenu("kitchen"); ?>><a href="<?php echo URL ?>kitchen#content">KITCHEN REMODEL</a></li>
							<li <?php $this->helpers->isActiveMenu("bathroom"); ?>><a href="<?php echo URL ?>bathroom#content">BATHROOM REMODEL</a></li>
							<li <?php $this->helpers->isActiveMenu("decksfencing"); ?>><a href="<?php echo URL ?>decksfencing#content">DECKS & FENCING</a></li>
							<li <?php $this->helpers->isActiveMenu("roofingsiding"); ?>><a href="<?php echo URL ?>roofingsiding#content">ROOFING & SIDING</a></li>
							<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">PRIVACY POLICY</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<div class="bnPanel">
					<p>Turn Your House Into Your Dream Home.</p>
					<p>A to Z Construction. One call does it ALL!!</p>
					<a href="#" class="btn">FREE ESTIMATE</a>
				</div>
			</div>
		</div>
	<?php //endif; ?>
