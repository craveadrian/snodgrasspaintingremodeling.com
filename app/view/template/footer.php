<footer>
	<div id="contact-details-section">
		<img src="public/images/content/blueArrow.png" alt="BlueArrow" class="sArrow">
		<div class="row">
			<div class="cdsLeft inbTop">
				<img src="public/images/common/cdEmail.png" alt="Email Icon">
				<p><?php $this->info(["email","mailto"]); ?></p>
			</div>
			<div class="cdsMid inbTop">
				<img src="public/images/common/cdPhone.png" alt="Phone Icon">
				<p><?php $this->info(["phone","tel"]); ?></p>
			</div>
			<div class="cdsRight inbTop">
				<img src="public/images/common/cdLocation.png" alt="Location Icon">
				<a href="https://www.google.com.ph/maps/place/Granbury,+TX+76049,+USA/@32.4546679,-97.859253,11z/data=!3m1!4b1!4m5!3m4!1s0x864e1f5815003ecd:0xf8a322bea321104c!8m2!3d32.4588558!4d-97.7199454" target="_blank"><p><?php $this->info("address"); ?></p></a>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="footTop">
			<ul>
				<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home | </a></li>
				<li <?php $this->helpers->isActiveMenu("painting"); ?>><a href="<?php echo URL ?>painting#content">Painting | </a></li>
				<li <?php $this->helpers->isActiveMenu("kitchen-remodel"); ?>><a href="<?php echo URL ?>kitchen-remodel#content">Kitchen Remodel | </a></li>
				<li <?php $this->helpers->isActiveMenu("bathroom-remodel"); ?>><a href="<?php echo URL ?>bathroom-remodel#content">Bathroom Remodel | </a></li>
				<li <?php $this->helpers->isActiveMenu("decks-and-fencing"); ?>><a href="<?php echo URL ?>decks-and-fencing#content">Decks & Fencing | </a></li>
				<li <?php $this->helpers->isActiveMenu("roofing-and-siding"); ?>><a href="<?php echo URL ?>roofing-and-siding#content">Roofing & Siding | </a></li>
				<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">Privacy Policy</a></li>
			</ul>
		</div>
		<div class="footBot">
			<p class="copy">
				© <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved.
				<?php if( $this->siteInfo['policy_link'] ): ?>
					<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>.
				<?php endif ?>
			</p>
			<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php //if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php //endif; ?>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>
