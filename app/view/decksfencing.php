<div id="content">
	<div class="row">
		<h1>DECKS AND FENCING</h1>
    <div class="inner-decks-and-fencing">
			<h2>Bring the party outside</h2>
			<p>Make your summer gatherings and family cookouts the best ever this year with a new deck with help from Snodgrass Painting & Remodeling. You'll be the envy of the entire block with your brand new deck.</p>

			<p>Enjoy the fresh air and soak up some rays with a quality porch or deck built by our highly-trained and truly-skilled professional crew. We’re also able to revamp existing decks and make them sparkle like you wouldn’t believe!</p>

			<h2>Make your home stand out with quality fences</h2>
			<p>Whether your main concern is keeping children and pets in and unwelcome guests out, or you just want to define boundaries and tie together the whole package, you can do it with quality fencing through Snodgrass Painting & Remodeling.</p>

			<p>Our highly-skilled crew can provide you with more than just a brand new fence. If you’re happy with the one you already have, we can work to refinish and paint or stain it to make it look brand-new all over again.</p>
    </div>
		<div id="gall1" class="gallery-container">
		<ul class="gallery clearfix" >
				<?php foreach ($gallery as $gall) {  if($gall["rel"] == "decks") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
	</div>
</div>
