<div id="content">
	<div class="row">
		<h1>KITCHEN REMODEL</h1>
    <div class="inner-kitchen-remodel">
    	<h2>Tired of your outdated cabinets and awkward layout?</h2>
			<p>It’s hard to relish in the joys that cooking can bring if the kitchen’s flow doesn’t work for you. Maybe the cabinets have you feeling stuck in a time warp. Perhaps you have to keep meals simple because there’s not enough counter space.</p>
			<p>Take charge and make your kitchen work for YOU. When you take advantage of our FREE estimates, Snodgrass Painting & Remodeling will sit down and help you figure out what works best for your space and your vision, and will also give you a low quote you’ll love.</p>
			<h2>Bring your dream kitchen to life with a variety of styles!</h2>
			<p>Although there may be nothing actually wrong with your kitchen, perhaps it just doesn't fit your style. It might be time for a remodel to make your kitchen match you and your family’s personality!</p>
			<p>Turn your boring, traditional kitchen into a modern delight that suits your contemporary flair! Let us transform your 1950s-era cooking space into a room that better matches the Victorian-style décor of the rest of your home. Your vision will be your reality!</p>
    </div>
		<div id="gall1" class="gallery-container">
		<ul class="gallery clearfix" >
				<?php foreach ($gallery as $gall) {  if($gall["rel"] == "kitchen") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
	</div>
</div>
