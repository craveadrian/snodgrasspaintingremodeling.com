<div id="content">
	<div id="welcome-section">
		<div class="row">
			<div class="wlcLeft inbTop">
				<h2>WELCOME</h2>
				<h1>SNODGRASS <span>PAINTING & REMODELING</span> </h1>
			</div>
			<div class="wlcRight inbTop">
				<p>Whether you’re looking for a light remodel or a complete overhaul, we can help. You’ll LOVE the quality service and attention to detail Snodgrass Painting and Remodeling can provide.</p>
			</div>
		</div>
	</div>
	<div id="service-section">
		<div class="row">
			<img src="public/images/content/blackArrow.png" alt="Black Arrow" class="sArrow">
			<h1>Our Services</h1>
			<h4>Be amazed at the difference a home update can make</h4>
			<div class="svcImg inbTop">
				<img src="public/images/content/img1.jpg" alt="Remodeling">
				<p class="service">REMODELING</p>
			</div>
			<div class="svcImg mid inbTop">
				<img src="public/images/content/img2.jpg" alt="Painting">
				<p class="service">PAINTING</p>
			</div>
			<div class="svcImg inbTop">
				<img src="public/images/content/img3.jpg" alt="Roofing">
				<p class="service">ROOFING</p>
			</div>
			<div class="svcBot">
				<div class="svcBotLeft inbTop">
					<div class="panel">
						<img src="public/images/common/phone.png" alt="Phone" class="inbMid">
						<p class="phone inbMid"><?php $this->info(["phone","tel"]); ?></p>
						<img src="public/images/content/img4.jpg" alt="Kitchen">
						<p class="service-quote">No job is too big or small for this hardworking crew</p>
					</div>
				</div>
				<div class="svcBotRight inbTop">
					<h2>Contact Us</h2>
					<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<label><span class="ctc-hide">Name:</span>
							<input type="text" name="name" placeholder="">
						</label>
						<label><span class="ctc-hide">Phone:</span>
							<input type="text" name="phone" placeholder="">
						</label>
						<label><span class="ctc-hide">Email:</span>
							<input type="text" name="email" placeholder="">
						</label>
						<label><span class="ctc-hide">Message/Questions:</span>
							<textarea name="message" cols="30" rows="10" placeholder=""></textarea>
						</label>
						<div class="g-recaptcha"></div>
						<label>
							<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
						</label><br>
						<?php if( $this->siteInfo['policy_link'] ): ?>
						<label>
							<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
						</label><br>
						<?php endif ?>
						<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="social-section">
		<div class="row">
			<div class="socialMedia fl">
				<p>Follow Us</p>
				<p class="social">
					<a href="<?php $this->info("fb_link"); ?>" class="socialico">f</a>
					<a href="<?php $this->info("tt_link"); ?>" class="socialico">l</a>
					<a href="<?php $this->info("yt_link"); ?>" class="socialico">x</a>
					<a href="<?php $this->info("rss_link"); ?>" class="socialico">r</a>
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="project-section">
		<div class="row">
			<img src="public/images/content/blackArrow.png" alt="Black Arrow" class="sArrow">
			<h1>Recent Projects</h1>
			<div class="project-Images"> <img src="public/images/content/project1.jpg" alt=""> </div>
			<div class="project-Images middle"> <img src="public/images/content/project2.jpg" alt=""> </div>
			<div class="project-Images"> <img src="public/images/content/project3.jpg" alt=""> </div>
			<div class="project-Images"> <img src="public/images/content/project4.jpg" alt=""> </div>
			<div class="project-Images middle"> <img src="public/images/content/project5.jpg" alt=""> </div>
			<div class="project-Images"> <img src="public/images/content/project6.jpg" alt=""> </div>
			<a href="<?php echo URL ?>" class="botLogo"><img src="public/images/common/botLogo.png" alt="SNODGRASS PAINTING AND REMODELING MAIN LOGO"></a>
		</div>
	</div>
</div>
