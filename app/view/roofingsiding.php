<div id="content">
	<div class="row">
		<h1>ROOFING AND SIDING</h1>
    <div class="inner-roofing-and-siding">
			<h2>Protect your home from the elements</h2>
			<p>When your roof is showing wear and tear, it’s time to act fast to make sure you and your family remain safe and protected from extremem weather. Damages as a result of a leaky roof can cost you thousands of dollars in repairs. Don’t let this happen to you!</p>

			<p>Snodgrass Painting & Remodeling can fix or replace your current roof and siding at a competitive price that won’t break the bank. When you take advantage of our FREE estimates, you can rest easy knowing that your home and wallet will be totally safe.</p>

			<h2>Don’t let your roof be an eyesore</h2>
			<p>While the main objective of roofing and siding is to protect you and your home from the elements, there’s a lot more to them than that, like what it looks like!</p>

			<p>Give your home the beautiful look you want that will have the neighbors green with envy. An update to your current roof or siding can be just what you need to give your home a look that is uniquely you.</p>
    </div>
		<div id="gall1" class="gallery-container">
		<ul class="gallery clearfix" >
				<?php foreach ($gallery as $gall) {  if($gall["rel"] == "roofing") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
	</div>
</div>
