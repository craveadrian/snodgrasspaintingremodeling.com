<div id="content">
	<div class="row">
		<h1>PAINTING</h1>
    <div class="inner-painting">
			<h2>First impressions are lasting impressions</h2>
			<p>It’s true that you shouldn’t judge a book by its cover, but that’s easier said than done. When your home’s exterior paint leaves something to be desired, passersbys and first-time visitors will often believe the inside is no different.</p>

			<p>Snodgrass Painting & Remodeling knows the importance of quality prep work and how to make your home stand out and make you proud. Understanding and know-how ensures that your new paint job makes an impression built to last a lifetime.</p>

			<h2>Faux finishes and wallpaper can make your home shine</h2>
			<p>In addition to traditional interior and exterior paint jobs, Snodgrass is also able to give your home or business a faux finish to match any texture your heart desires. When a faux finish isn’t your cup of tea, you’ll also be able to take advantage of our wallpapering services.</p>

			<p>FREE estimates mean you won’t have to deal with a surprise bill when the job is done. Accurate quotes give you a very close ballpark figure so you can sit back and relax while your home or office is transformed, instead of worrying about how much you’ll end up paying.</p>
    </div>
		<div id="gall1" class="gallery-container">
		<ul class="gallery clearfix" >
				<?php foreach ($gallery as $gall) {  if($gall["rel"] == "painting") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
	</div>
</div>
